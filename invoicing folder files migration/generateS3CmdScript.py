try:
    # for Python 2.x
    from StringIO import StringIO
except ImportError:
    # for Python 3.x
    from io import StringIO
import csv



def csv_dict_reader(file_obj):
    reader = csv.reader(file_obj, delimiter=',');
    file = open("results.csv","w")
    for row in reader:
    	bashCommand = "s3cmd cp  s3://rivigo-accounting/invoicing/invoice/"+ row[0]+"-INVOICE-"+row[1]+".pdf"+ " s3://rivigo-accounting/invoicing/"+row[1]+"/; \n"
        bashCommand += "s3cmd cp s3://rivigo-accounting/invoicing/annexure/"+ row[0]+"-ANNEXURE-"+row[1]+".csv"+ " s3://rivigo-accounting/invoicing/"+row[1]+"/; \n"
        bashCommand += "s3cmd cp s3://rivigo-accounting/invoicing/annexurePdf/"+ row[0]+"-ANNEXURE-"+row[1]+".pdf"+ " s3://rivigo-accounting/invoicing/"+row[1]+"/;"
        print(bashCommand)
        import subprocess
        try:
            output = subprocess.check_output(['bash','-c', bashCommand])
            print("Output: ");
            print output;
            file.write(row[0]+","+",".join(output.split("\n"))+"\n");
        except subprocess.CalledProcessError as e:
            print("command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output))
            file.write(row[0]+","+"command '{}' return with error (code {}): {}".format(e.cmd, e.returncode, e.output)+"\n");

    print("\n");
    file.close() 


#----------------------------------------------------------------------
if __name__ == "__main__":
    with open("Attachments1.csv") as f_obj:
        csv_dict_reader(f_obj)






